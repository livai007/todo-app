import { Component, OnInit } from '@angular/core';
import { TodoI } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  expandedTodoListDone = false

  constructor(private todoService: TodoService) { }

  get todoListNotDone(): TodoI[] {
    return this.todoService.todoListNotDone
  }
  get todoListDone(): TodoI[] {
    return this.todoService.todoListDone
  }
  get todoListLoading(): boolean {
    return this.todoService.loading
  }

  ngOnInit(): void {
    this.todoService.load()
  }

}
