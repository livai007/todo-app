import { MatDialog } from '@angular/material/dialog';
import { ConfirmDeleteTodoDialogComponent } from './../confirm-delete-todo-dialog/confirm-delete-todo-dialog.component';
import { TodoService } from 'src/app/services/todo.service';
import { Component, Input } from '@angular/core';
import { TodoI } from 'src/app/models/todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent{

  @Input() todo: TodoI | null = null
  loadingAction = false
  modeAdd = false

  constructor(private todoService: TodoService, public dialog: MatDialog) {}

  onMarkAsCompleted() {
    if (this.todo) {
      this.loadingAction = true;
      this.todoService.markAsCompleted(this.todo)
        .finally(() => {
          this.loadingAction = false;
        });
    }
  }
  onMarkAsNoCompleted() {
    if (this.todo) {
      this.loadingAction = true;
      this.todoService.markAsNoCompleted(this.todo)
        .finally(() => {
          this.loadingAction = false;
        });
    }
  }
  onUpdate() {}

  onDelete() {
    this.dialog.open(ConfirmDeleteTodoDialogComponent, {
      width: '350px',
      data: {
        todo: this.todo
      }
    });
  }
}
