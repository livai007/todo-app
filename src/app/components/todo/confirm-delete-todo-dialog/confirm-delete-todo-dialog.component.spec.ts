import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteTodoDialogComponent } from './confirm-delete-todo-dialog.component';

describe('ConfirmDeleteTodoDialogComponent', () => {
  let component: ConfirmDeleteTodoDialogComponent;
  let fixture: ComponentFixture<ConfirmDeleteTodoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmDeleteTodoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteTodoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
