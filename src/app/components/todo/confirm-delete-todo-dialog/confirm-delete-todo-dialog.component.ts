import { TodoI } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete-todo-dialog',
  templateUrl: './confirm-delete-todo-dialog.component.html',
  styleUrls: ['./confirm-delete-todo-dialog.component.scss']
})
export class ConfirmDeleteTodoDialogComponent {

  loading = false
  constructor(
    public dialogRef: MatDialogRef<ConfirmDeleteTodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {todo: TodoI},
    private todoService: TodoService
  ) {}

  onConfirm() {
    if (this.data.todo) {
      this.loading = true;
      this.todoService.delete(this.data.todo)
          .then(() => {
            this.dialogRef.close();
          })
          .finally(() => {
            this.loading = false;
          });
    }
  }
}
