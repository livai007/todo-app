import { MatSnackBar } from '@angular/material/snack-bar';
import { TodoService } from 'src/app/services/todo.service';
import { TodoI } from 'src/app/models/todo';
import { Component, EventEmitter, Input, Output, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss']
})
export class TodoInputComponent implements OnInit {

  @Input() todo: TodoI | null = null
  @Output() cancelEvent: EventEmitter<null> = new EventEmitter();

  newTodoLabel: string = ''
  loadingAction = false

  constructor(private todoService: TodoService, private _snackBar: MatSnackBar) {}

  @ViewChild('input', { static: false })
  set input(element: ElementRef<HTMLInputElement>) {
    setTimeout(() => {
      element.nativeElement.focus()
    }, 0);
  }

  ngOnInit(): void {
    this.newTodoLabel = this.todo?.todo_label || ''
  }

  onConfirm() {
    this.loadingAction = true;
    if (!this.todo) {
      this.todoService.store({
        todo_label: this.newTodoLabel,
        todo_is_done: 0
      })
      .then(() => {
        this.onCancel()
      })
      .finally(() => {
        this.loadingAction = false;
      });
    } else {
      this.todoService.update(
        {
          ...this.todo,
          todo_label: this.newTodoLabel
        },
        () => {
          this._snackBar.open("Tâche mise à jour avec succès", "ok", {
            duration: 3000
          })
        }
      )
      .then(() => {
        this.onCancel()
      })
      .finally(() => {
        this.loadingAction = false;
      });
    }
  }

  onCancel() {
    this.cancelEvent.emit()
  }
}
