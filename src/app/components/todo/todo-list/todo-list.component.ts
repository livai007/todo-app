import { Component, Input } from '@angular/core';
import { TodoI } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {

  @Input() todoList: TodoI[] = []

  constructor() {}

}

