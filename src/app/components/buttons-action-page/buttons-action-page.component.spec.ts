import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsActionPageComponent } from './buttons-action-page.component';

describe('ButtonsActionPageComponent', () => {
  let component: ButtonsActionPageComponent;
  let fixture: ComponentFixture<ButtonsActionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonsActionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsActionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
