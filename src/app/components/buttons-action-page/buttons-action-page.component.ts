import { ConfirmLogoutDialogComponent } from './../confirm-logout-dialog/confirm-logout-dialog.component';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-buttons-action-page',
  templateUrl: './buttons-action-page.component.html',
  styleUrls: ['./buttons-action-page.component.scss']
})
export class ButtonsActionPageComponent {

  modeAdd = false;
  constructor(public dialog: MatDialog) { }

  onLogout() {
    this.dialog.open(ConfirmLogoutDialogComponent, {
      width: '350px'
    });
  }
}
