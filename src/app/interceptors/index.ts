import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpClientInterceptor } from './http-client.interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [{ provide: HTTP_INTERCEPTORS, useClass: HttpClientInterceptor, multi: true }];
