import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';

import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let clonedRequest = request.clone();

    const noApiRequest = request.headers.get('no-api-request');
    if (!noApiRequest) {
      clonedRequest = clonedRequest.clone({
        url: environment.apiUrl + request.url
      });
    }
    if (!noApiRequest && this.authService.isAuthenticated()) {
      clonedRequest = clonedRequest.clone({
        headers: clonedRequest.headers
          .set('Authorization', `Bearer ${this.authService.token}`)
          .set('user-id', this.authService.user?.id.toString() || '')
      });
    }

    return next.handle(clonedRequest).pipe(
      tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
              this.handleResponse(event);
          }
      }),
      catchError((responseError: HttpErrorResponse) => {
          this.handleResponseError(responseError);
          return throwError(() => responseError)
      })
    );
  }

  private handleResponse(response: HttpResponse<any>): void {
    // to implement
  }

  private async handleResponseError(response: HttpErrorResponse): Promise<any> {
    if (response.status === 0) {
      this.presentSnackBar('Veuillez vérifier votre connexion internet');
    }
    else if (response.status === 403) {
      this.presentSnackBar('Vous n\'êtes pas autorisé a effectué cette action');
    }
    else if (response.status === 401) {
      this.presentSnackBar('Veuillez vous reconnectez pour procéder à cette action');
      this.router.navigate(['login'])
    }
  }

  presentSnackBar(message: string) {
    this._snackBar.open(message, "ok", {
      duration: 3000
    });
  }

}
