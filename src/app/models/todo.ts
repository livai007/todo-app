export interface TodoI {
  todo_id?: number;
  todo_user_id?: number;
  todo_label: string;
  todo_date?: string;
  todo_is_done: number;
}
