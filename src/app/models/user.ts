export interface UserI {
  id: number;
  name: string;
}

export interface UserLoginResponseI {
  token?: string;
  user?: UserI;
}

export interface UserLoginPayloadI {
  login: string;
  password: string;
}
