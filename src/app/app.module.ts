import { AngularMaterialModule } from './modules/angular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { httpInterceptorProviders } from './interceptors';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { ButtonsActionPageComponent } from './components/buttons-action-page/buttons-action-page.component';
import { ConfirmLogoutDialogComponent } from './components/confirm-logout-dialog/confirm-logout-dialog.component';
import { TodoComponent } from './components/todo/todo/todo.component';
import { TodoListComponent } from './components/todo/todo-list/todo-list.component';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { TodoInputComponent } from './components/todo/todo-input/todo-input.component';
import { ConfirmDeleteTodoDialogComponent } from './components/todo/confirm-delete-todo-dialog/confirm-delete-todo-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    ButtonsActionPageComponent,
    ConfirmLogoutDialogComponent,
    TodoComponent,
    TodoListComponent,
    TodoInputComponent,
    ConfirmDeleteTodoDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatProgressBarModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
