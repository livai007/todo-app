import { Router } from '@angular/router';
import { UserI } from './../models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLoginPayloadI, UserLoginResponseI } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _user: UserI | null = null;

  constructor(
    private httpClient: HttpClient,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    const userFromLocalStorage = localStorage.getItem('user')
    if(userFromLocalStorage) {
      this._user = JSON.parse(userFromLocalStorage)
    }
  }

  get token() {
    return localStorage.getItem('token');
  }

  get user(): UserI | null {
    return this._user
  }

  get username(): string {
    if (this.user) {
      return this.user.name
    }
    return ''
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !!token;
  }

  public login(payload: UserLoginPayloadI): any {
    return new Promise<any>((resolve, reject) => {
      this.httpClient.post('auth/login', payload)
        .subscribe({
          next: (userLoginResponse: UserLoginResponseI) => {
            this.saveLoginDataToLocalStorage(userLoginResponse)
            resolve(userLoginResponse);
          },
          error: (errorResponse) => {
            if (errorResponse.status === 401) {
              this._snackBar.open('Identifiants non reconnu', 'ok', {
                duration: 3000
              });
            }
            reject(errorResponse)
          }
        });
    })
  }

  private saveLoginDataToLocalStorage(userLoginResponse: UserLoginResponseI) {
    if (userLoginResponse.token) {
      localStorage.setItem('token', userLoginResponse.token);
    }
    if (userLoginResponse.user) {
      this._user = userLoginResponse.user
      localStorage.setItem('user', JSON.stringify(userLoginResponse.user));
    }
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    this._user = null
    this.router.navigate(['login'])
  }
}
