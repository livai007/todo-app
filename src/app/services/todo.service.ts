import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TodoI } from './../models/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private _loading = false
  private _todoList: TodoI[] = []

  constructor(private httpClient: HttpClient, private _snackBar: MatSnackBar) { }

  get todoListDone(): TodoI[] {
    return this._todoList.filter(todo => todo.todo_is_done)
  }

  get todoListNotDone(): TodoI[] {
    return this._todoList.filter(todo => !todo.todo_is_done)
  }

  get loading(): boolean {
    return this._loading
  }

  public load(): void {
    this._loading = true
    this.httpClient.get('todo/list')
      .subscribe({
        next: ((todoList: any) => {
          this._todoList = todoList
        }),
        complete: ((): void => {
          this._loading = false
        })
      })
  }

  public update(todo: TodoI, callbackSuccess: Function, callbackFailed?: Function): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.httpClient.post('todo', todo)
        .subscribe({
          next: ((response) => {
            resolve(response)
            if (callbackSuccess) {
              callbackSuccess()
            }
            this.load()
          }),
          error: ((): void => {
            reject()
            if (callbackFailed) {
              callbackFailed()
            } else {
              this._snackBar.open("L'operation n'a pas aboutie", "Ok", {
                duration: 3000
              });
            }
          })
        })
    })
  }

  public markAsCompleted(todo: TodoI):  Promise<any> {
    return this.update(
      {
        ...todo,
        todo_is_done: 1
      },
      () => {
        this._snackBar.open("Tâche complétée", "ok", {
          duration: 3000
        });
      }
    )
  }

  public markAsNoCompleted(todo: TodoI):  Promise<any> {
    return this.update(
      {
        ...todo,
        todo_is_done: 0
      },
      () => {
        this._snackBar.open("Tâche marqué comme étant non complétée", "ok", {
          duration: 3000
        });
      }
    )
  }

  public delete(todo: TodoI): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.httpClient.delete(`todo/${todo.todo_id}`)
        .subscribe({
          next: ((response) => {
            this.load()
            this._snackBar.open("Tâche supprimée avec succès", "ok", {
              duration: 3000
            });
            resolve(response)
          }),
          error: ((): void => {
            reject()
            this._snackBar.open("L'operation n'a pas aboutie", "Ok", {
              duration: 3000
            });
          })
        })
    })
  }

  public store(todo: TodoI): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.httpClient.post('todo', todo)
        .subscribe({
          next: ((response) => {
            this.load()
            this._snackBar.open("Tâche ajoutée avec succès", "ok", {
              duration: 3000
            });
            resolve(response)
          }),
          error: ((): void => {
            reject()
            this._snackBar.open("L'operation n'a pas aboutie", "Ok", {
              duration: 3000
            });
          })
        })
    })
  }
}
